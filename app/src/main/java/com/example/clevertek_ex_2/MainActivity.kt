package com.example.clevertek_ex_2

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.app.Instrumentation
import android.content.*
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.ContactsContract
import android.text.TextUtils
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.edit
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.room.*
import com.example.clevertek_ex_2.data.Contact
import com.example.clevertek_ex_2.data.ContactViewModel
import com.example.clevertek_ex_2.data.UserDao
import java.lang.Exception
import java.util.*
import java.util.jar.Manifest
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    lateinit var buttonNameAdd: Button
    lateinit var buttonShowContact: Button //= findViewById(R.id.showContact)
    lateinit var buttonShowFromSP: Button //= findViewById(R.id.showContactsFromSp)
    lateinit var nameTXT: TextView
    lateinit var emailTXT: TextView
    lateinit var phoneTXT: TextView
    private lateinit var mContactViewModel: ContactViewModel
    lateinit var db: RoomDatabase
    lateinit var sp: SharedPreferences



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nameTXT = findViewById(R.id.nameText)
        emailTXT = findViewById(R.id.emailText)
        phoneTXT = findViewById(R.id.phoneText)
        buttonNameAdd = findViewById(R.id.addContact)
        buttonShowContact = findViewById(R.id.showContact)
        buttonShowFromSP = findViewById(R.id.showContactsFromSp)



        val


        mContactViewModel = ViewModelProvider(this).get(ContactViewModel::class.java)

        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-name"
        ).allowMainThreadQueries().build()

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS)
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    android.Manifest.permission.READ_CONTACTS
                )
            ) {
                buttonNameAdd.isEnabled = false
                Toast.makeText(this, "Please grand access to contacts", Toast.LENGTH_LONG).show()
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(android.Manifest.permission.READ_CONTACTS), 200
                )
            }
        }



        val startForResult =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
                if (result.resultCode == Activity.RESULT_OK) {
                    val intent = result.data
                    val contactUri = intent?.data
                    val contentResolver = contentResolver
                    var stringID = ""

                    val cursor = contactUri?.let {
                        contentResolver.query(
                            it,
                            arrayOf(
                                ContactsContract.Contacts._ID,
                                ContactsContract.Contacts.DISPLAY_NAME,
                            ),
                            null, null, null
                        )
                    }

                    cursor.use { cursor ->
                        if (cursor != null) {
                            cursor.moveToFirst()
                            stringID = cursor.getString(0)
                            nameTXT.text = cursor.getString(1)
                        }
                    }

                    val cursor1 = ContactsContract.CommonDataKinds.Email.CONTENT_URI.let {
                        contentResolver.query(
                            it,
                            arrayOf(ContactsContract.CommonDataKinds.Email.ADDRESS),
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?" ,
                            arrayOf(stringID),
                            null
                        )
                    }
                    cursor1.use { cursor ->
                        if (cursor != null) {
                            cursor.moveToFirst()
                            emailTXT.text = cursor.getString(0)
                        }
                    }

                    val cursor2 = ContactsContract.CommonDataKinds.Phone.CONTENT_URI.let {
                        contentResolver.query(
                            it,
                            arrayOf(ContactsContract.CommonDataKinds.Phone.NUMBER),
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            arrayOf(stringID),
                            null
                        )
                    }

                    cursor2.use { cursor ->
                        if (cursor != null) {
                            cursor.moveToFirst()
                            phoneTXT.text = cursor.getString(0)
                        }
                    }
                    val name = nameTXT.text.toString()
                    val email = emailTXT.text.toString()
                    val userPhone = phoneTXT.text.toString()
                    val userDao = db.userDao()

                    if (inputCheck(name, email, userPhone)){
                        val contact = Contact(0, name, email, userPhone)

                            userDao.addContact(contact)
                        Toast.makeText(this, "Successfully added", Toast.LENGTH_LONG).show()
                    }
                }
            }

        fun openActivityForResult() {
            val intent = Intent(Intent.ACTION_PICK)
            intent.data = ContactsContract.Contacts.CONTENT_URI
            startForResult.launch(intent)
        }

        buttonNameAdd.setOnClickListener {
            openActivityForResult()
        }



        buttonShowContact.setOnClickListener {
            val builder = AlertDialog.Builder(this)

            val userDao = db.userDao()
            val listContacts = userDao.getAllPhones()
            Log.i("123", listContacts[0])
            builder.setTitle("All Numbers")
            builder.setItems(listContacts){dialog,wich ->
                Toast.makeText(this, "DISPLAYED", Toast.LENGTH_SHORT).show()
                val selectNum = listContacts[wich]
                val infContact = userDao.findByNumber(selectNum)
                nameTXT.text = infContact.Name
                emailTXT.text = infContact.userEmail
                phoneTXT.text = infContact.userPhone
                sp = getSharedPreferences("SP", MODE_PRIVATE)
                val editor = sp.edit()
                editor.apply {
                    putString("PHONE_NUM", selectNum)
                }.apply()
            }
            builder.show()

        }

        buttonShowFromSP.setOnClickListener {
            val stringSP = sp.getString("PHONE_NUM", "Not Contact")
            nameTXT.text = "Contact From SP"
            emailTXT.text = "Contact From SP"
            phoneTXT.text = stringSP
        }



    }


//    private fun createDialogAlert() {
//        val builder = AlertDialog.Builder(this)
//        val listContacts = mContactViewModel.readPhones()
//        builder.setTitle("All Numbers")
//        builder.setItems(listContacts) { dialog, which ->
//                Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show()
//
//        }
//    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 200) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                buttonNameAdd.isEnabled = false
            }
        }
    }


//    private fun createDialogAlert(){
//        val builder = AlertDialog.Builder(this)
//        val listContacts = mContactViewModel.readPhones()
//        builder.setTitle("AllContacts")
//        builder.setMessage("aaaa")
//        builder.setMultiChoiceItems(R.array.multi_ittem, null,)
//        builder.setNeutralButton("OK"){ dialogInterface, i ->
//
//        }
//        builder.show()
//    }

//    private fun insertDataToDatabase() {
//        val name = nameTXT.text.toString()
//        val email = emailTXT.text.toString()
//        val userPhone = phoneTXT.text.toString()
//
//        if (inputCheck(name, email, userPhone)){
//            val contact = Contact(0, name, email, userPhone)
//            .addContact(contact)
//            Toast.makeText(this, "Successfully added", Toast.LENGTH_LONG).show()
//        }
//
//    }

    private fun inputCheck(name: String, email: String, userPhone: String): Boolean{
        return !(TextUtils.isEmpty(name) && TextUtils.isEmpty(email) && TextUtils.isEmpty(userPhone))
    }

    @Database(entities = arrayOf(Contact::class), version = 1)
    abstract class AppDatabase : RoomDatabase() {
        abstract fun userDao(): UserDao
    }


}





//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        if(requestCode == 0 && data != null){
//            val contactUri = data.data
//            val contentResolver = contentResolver
//            var stringID = ""
//
//            val cursor = contactUri?.let {
//                contentResolver.query(
//                    it,
//                    arrayOf(ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME),
//                    null, null, null)
//            }
//
//            cursor.use { cursor ->
//                if (cursor != null) {
//                    cursor.moveToFirst()
//                    stringID = cursor.getString(0)
//                    nameTXT.text = cursor.getString(1)
//                }
//            }
//
//            val cursor1 = ContactsContract.CommonDataKinds.Email.CONTENT_URI.let {
//                contentResolver.query(
//                    it,
//                    arrayOf(ContactsContract.CommonDataKinds.Email.ADDRESS),
//                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", arrayOf(stringID), null)
//            }
//            cursor1.use { cursor1 ->
//                if (cursor1 != null) {
//                    cursor1.moveToFirst()
//                    stringID = cursor1.getString(0)
//                    emailTXT.text = cursor1.getString(1)
//                }
//            }
//        }
//
//
//        }


