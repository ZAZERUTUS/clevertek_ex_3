package com.example.clevertek_ex_2.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Contact (
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "name") val Name: String?,
    @ColumnInfo(name = "email") val userEmail: String?,
    @ColumnInfo(name = "numPhone") val userPhone: String?
)