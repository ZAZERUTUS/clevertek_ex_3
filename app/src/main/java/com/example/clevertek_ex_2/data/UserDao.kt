package com.example.clevertek_ex_2.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface UserDao {
    @Insert
    fun addContact(vararg contact: Contact)

    @Query("SELECT * FROM contact")
    fun getAll(): LiveData<List<Contact>>

    @Query("SELECT numPhone FROM contact")
    fun getAllPhones(): Array<String>

//    @Query("SELECT * FROM user WHERE uid IN (:userIds)")
//    fun loadAllByIds(userIds: IntArray): User

    @Query("SELECT * FROM contact WHERE numPhone LIKE :phone")
    fun findByNumber(phone: String): Contact

//    @Insert
//    fun insertAll(vararg users: User)

    @Delete
    fun delete(user: Contact)}
