package com.example.clevertek_ex_2.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ContactViewModel(application: Application): AndroidViewModel(application) {
    private val getAll: LiveData<List<Contact>>
    private val repository: ContactRepository

    init {
        val userDao = ContactsDatabase.getDatabase(application).userDao()
        repository = ContactRepository(userDao)
        getAll = repository.readAllData
    }

    fun addContact(contact: Contact) {
        viewModelScope.launch (Dispatchers.IO) {
            repository.addContact(contact)
        }
    }

    fun readPhones(): Array<String> {
             return repository.readNumContacts()
    }
}