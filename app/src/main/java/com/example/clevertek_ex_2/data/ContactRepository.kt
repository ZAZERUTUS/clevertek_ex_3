package com.example.clevertek_ex_2.data

import androidx.lifecycle.LiveData

class ContactRepository(private val userDao: UserDao) {
    val readAllData: LiveData<List<Contact>> = userDao.getAll()
    suspend fun addContact(contact: Contact){
        userDao.addContact(contact)

    }
    fun readNumContacts(): Array<String> {
        return userDao.getAllPhones()

    }
}